#项目管理
Everything could be Project.
## DAY 1
### 什么是项目？
PMBOK 6th : 为创造独特的产品、服务或者成果而进行的临时性工作。  
产品：研发项目  
服务：交付项目  
成果: 变革项目
### 项目管理的行业演变及发展
过去20年，从乙方到甲方，从低端到顶端，从红海到蓝海，从传统到现代。  
施工、设计、通讯、软件、制造、能源、房地产、医药、金融、互联网。  
推动项目管理普及的力量：1，竞争加剧、利润走薄 2，客户响应要求提高 3， 更新换代速度加快 4， 企业内部管理变革。  
便宜 && 快 -> 丑  
便宜 && 好 -> 等  
快 && 好 ->贵   
便宜 && 快 && 好 -> 滚  

多     快     好      省       
范围   时间   质量     成本  
Scope Time   Quality  Cost   

### 项目管理在企业中的价值
### 项目管理的三个基本目标
在规定的**时间**内，在批准的**预算**中，完成事先确定的工作**范围**内的工作，并且达到预期的**质量**性能要求。

项目工期与成本的关系，项目预算，合理工期，最优工期。  
