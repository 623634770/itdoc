# java 调优工具

### 1 jdk 命令行
#### 1.1 jps
> jps : java Virtual Machine Process Status Tool  
查看java进程，相当于Linux下的ps命令，只不过它只列出java进程。  

```
jps : 列出java程序进程ID和Main函数名称

jps -q : 只输出进程ID

jps -m : 输出传递给Java进程（主函数）的参数

jps -l : 输出主函数的完整路径

jps -v : 显示传递给Java虚拟的参数
```

#### 1.2 jstat
jstat : JVM Statistics Monitoring Tool  
jstat 可以查看Java 程序运行时相关信息，可以通过它查看堆的相关参数  

```
jstat -<options> [-t] [-h<lines>] <vmid> [<interval> [count]]
```

```
options:
-class: 显示ClassLoader的相关信息
-compiler: 显示JIT编译的相关信息
-gc: 显示与GC相关信息
-gccapacity: 显示各个代的容量和使用情况
-gccause: 显示垃圾收集相关信息（同-gcutil）,同时显示最后一次或当前正在发生的垃圾收集的诱发原因
-gcnew: 显示新生代信息
-gcnewcapacity: 显示新生代大小和使用情况
-gcold: 显示老年代信息
-gcoldcapacity: 显示老年代大小
-gcpermcapacity: 显示永久代大小
-gcutil: 显示垃圾收集信息
-gcintcompilation: 输出JIT编译的方法信息
-t: 在输出信息前加上一个Timestamp列，显示程序的运行时间
-h: 可以在周期性数据输出后，输出多少行数据后，跟着一个表头信息

interval: 用于指定输出统计数据的周期，单位为毫秒
count: 用于指定一个输出多少次数据

```

```
S0 年轻代中第一个survivor（幸存区）已使用的占当前容量百分比
S1 年轻代中第二个survivor（幸存区）已使用的占当前容量百分比
E 年轻代中Eden（伊甸园）已使用的占当前容量百分比
O old代已使用的占当前容量百分比
M metaspace已使用的占当前容量百分比
CCS 压缩使用比例
YGC 从应用程序启动到采样时年轻代中的gc次数
YGCT 从应用程序启动到采样时年轻代中的gc所用时间(s)
FGC 从应用程序启动到采样时old代(full gc)gc次数
FGCT 从应用程序启动到采样时old代(full gc)所用时间(s)
GCT 从应用程序启动到采样时gc用的总时间(s)

```
jstat -class <port>

```
Loaded : 已经装载类的数量
Bytes : 装载类所占用的字节数
Unloaded : 已经卸载类的数量
Bytes : 卸载类的字节数
Time : 装载和写在所花费的时间
```

#### 1.3 jinfo 
jinfo : Java Configuration Info
jinfo 可以用来查看正在运行的java程序的扩展参数，甚至支持运行时，修改部分参数

```
jinfo [option] <pid>
```

```
-flag <name> to print the value of the named VM flag
-flag [+|-]<name> to enable or disable the name VM flag
-flag <name>=<value> to set the named VM flag to the given value
-flags to print VM flags
-sysprops to print Java system properties
<no option> to print both of the above
-h | -help to print this help message
```

#### 1.4 jmap 
jmap : Memory Map  
jmap 用来查看堆内存使用状况，一般结合Jhat使用。
jmap语法格式如下：
```
Usage:
    jmap [option] <pid>
        (to connect to running process)
    jmap [option] <executable <core>
        (to connect to a core file)
    jmap [option] [server_id@]<remote server IP or hostname>
        (to connect to remote debug server)

where <option> is one of:
    <none>               to print same info as Solaris pmap
    -heap                to print java heap summary
    -histo[:live]        to print histogram of java object heap; if the "live"
                         suboption is specified, only count live objects
    -clstats             to print class loader statistics
    -finalizerinfo       to print information on objects awaiting finalization
    -dump:<dump-options> to dump java heap in hprof binary format
                         dump-options:
                           live         dump only live objects; if not specified,
                                        all objects in the heap are dumped.
                           format=b     binary format
                           file=<file>  dump heap to <file>
                         Example: jmap -dump:live,format=b,file=heap.bin <pid>
    -F                   force. Use with -dump:<dump-options> <pid> or -histo
                         to force a heap dump or histogram when <pid> does not
                         respond. The "live" suboption is not supported
                         in this mode.
    -h | -help           to print this help message
    -J<flag>             to pass <flag> directly to the runtime system
```

**参数**
```
option: 选项参数。
pid: 需要打印配置信息的进程ID。
executable: 产生核心dump的java可执行文件。
core: 需要打印配置信息的核心文件。
server-id: 可选的唯一id，如果相同的远程主机上运行了多台调试服务器，用此项参数标识服务器。
remote server IP or hostname 远程调试服务器的IP地址或主机名。 

option 
no option: 查看进程的内存映像信息，类似Solaris pmap 命令。 
heap: 显示Java堆详细信息。
histo[:liver]: 显示堆中对象的统计信息
clstats: 打印类加载器信息
finalizerinfo: 显示在F-Queue队列等待Finalizer线程执行finalizer方法的对象
dump:<dump-options>: 生成堆转储快照
F: 当-dump没有响应时，使用-dump 或者-histo 参数。在这个模式下，live子参数无效。 
help: 打印帮助信息
J<flag>: 指定传递给运行jmap的JVM的参数
```

**示例一：heap**  
命令：jmap -heap <pid>  
描述：显示java堆详细信息  

**示例二：histo[:live]**  
命令：jmap -histo:live <pid>  
描述：显示对重对象的统计信息   
其中包括每个Java类、对象数量、内存大小（单位：字节）、完全限定的类名。打印的虚拟机内部的类名称将会带有一个’*‘前缀。如果jar指定了live 子选项，则只计算活动的对象。 

**示例三：clstats**  
命令：jmap -clstats <pid>  
描述：打印类加载器信息  

-clstats是-permstat的替代方案，在JDK8之前，-permstat用来打印类加载器的数据  
打印java堆内存的永久保存区域的类加载器的智能统计信息。对于每个类加载器而言，它的名称、活跃度、地址、父类加载器、它所加载的类的数量和大小都会被打印。此外，包含的字符串数量和大小也会被打印。


**示例四：finalizerinfo**  
命令：jmap -finalizerinfo <pid>  
描述：打印等待终结的对象信息  
Number of objects pending for finalization: 0 说明当前F-QUEUE队列中并没有等待Fializer线程执行final  

**示例五：dump:<dump-options>**  
命令：jmap -dump:format=b,file=heapdump.phrof <pid>  
描述：生成堆转储快照dump文件。  
以hprof二进制格式转储java堆到指定filename的文件中。live子选项是可选的。如果指定了live子选项。堆中只有活动的对象会被转储。想要浏览heap dump,你可以使用jhat(java堆分析工具)读取生成的文件。  
这个命令执行，JVM会将整个heap的信息dump写入到一个文件，heap如果比较大的话，就会导致这个过程比较耗时，并且执行的过程中为了保证dump的信息是可靠的，所以会暂停应用，线上系统慎用。  


**
  
打印一个堆的摘要信息，包括使用的GC算法、堆配置信息和各内存区域内存使用信息

#### 1.5 jhat 
jhat : Java Heap Analysis Tool, jhat 命令解析Java堆转储文件，并启动一个web server. 然后用浏览器来查看/浏览dump出来的heap. jhat 命令支持预先设计的查询，比如显示某个类的所有实例。还支持 对象查询语言（OQL,Object Query Language）。OQL有点类似SQL，专门用来查询堆转储。OQL相关的帮助信息可以在jhat命令所提供的服务器页面最底部。如果使用默认端口，则OQL帮助信息页为： http://localhost:7000/oqlhelp/

java 生成堆转储的方式有多种

* 使用```jmap -dump```选项可以在JVM运行时获取 heap dump 。
* 使用```jconsole```选项通过HotSportDiagnosticMXBean从运行时获取堆转储。
* 在虚拟机启动时如果指定了```+XX:+HeapDumpOnOutOfMemoryError```选项，则抛出 OutOfMemoryError时，会自动执行堆转储。
* 使用```hprof```命令。
```
jhat [options] heap-dump-file 
```
参数： 
* options 可选命令行参数，请参考下面的options
* heap-dump-file 要查看的二进制Java堆转储文件（Java binary heap dump file）,如果某个转储文件中包含了 


jhat 启动后显示的html页面中包含有  
* All classes including platform : 显示出堆中所包含的所有的类
* Show all members of the rootset : 从跟集能引用到的对象
* Show instance counts for all classes(including platform/excluding platform) : 显示平台包含的所有类的实例数量
* Show heap histogram : 堆实例的分布表
* Show finalizer summary : Finalizer 摘要
* Execute Object Query Language（OQL) query: 执行对象查询语句（OQL）

#### 1.6 jstack

jstack : Java Stack Track, jstack 是Java虚拟机自带的一种堆栈跟踪工具。jstack 用于
生成java虚拟机当前时刻的线程快照。线程快照是当前java虚拟机内每一条线程正在执行的方法堆栈的集合。 
生成线程快照的主要目的是定位线程出现长时间停顿的原因，如线程间死锁，死循环，请求外部资源
导致的长时间等待等。线程出现停顿的时候通过jstack来查看各个线程的调用堆栈，就可以知道没有响应
的线程到底在后台做了什么事情，或者等待什么资源。如果java程序崩溃生成core文件，jstack工具可以
用来获取core文件的java stack和native stack的信息，从而可以轻松地知道java程序是如何崩溃和在程序何处发生问题。另外，jstack
工具还可以附属到正在运行的java程序中，看到当时运行的java程序的java stack和native stack的信息，如果现在运行的java 程序呈现
hung的状态，jstack是非常有用的。
在thread dump 中，要留意下面几种状态
* 死锁， Deadlock(重点关注)
* 等待资源， Waiting on condition (重点关注)
* 等待获取监视器， Waiting on monitor entry (重点关注)
* 阻塞， Blocked  (重点关注)
* 执行中， Runnable 
* 暂停， Suspended 
* 对象等待中， Object.wait() 或 TIMED_WAITING
* 停止， Parked 

使用方法
```
jstack [option] pid 查看当前时间点，指定进程的dump堆栈信息。
jstack [option] pid > 文件 将当前时间点的指定进程的dump堆栈信息写入到指定文件中。注：若该文件不存在，则会自动生成；若该文件存在，则会覆盖源文件。
jstack [option] executable core 查看当前时间点，core文件dump堆栈信息。
jstack [option] [server_id@]<remote server ip or hostname> 查看当前时间点，远程机器的dump堆栈信息。
```

将指定进程的当前堆栈情况记录到某个文件中：
```
$jstack -l <pid> > jstack_info.txt
```

**示例二：统计线程数**
```
$ jstack -l <pid> | grep 'java.lang.Thread.State' | wc -l
```
#### 1.7 jconsole 

jconsole : Java Monitoring and Management Console, java 5 引入， 一个内置java性能分析器， 可以从命令行或在GUI shell 中运行。 您可以轻松地使用jconsole来监控java应用程序性能和跟踪java中的代码。

如何启动jconsole
如果是从命令行启动， 使JDK在PATH上，运行jconsole即可。
如果从GUI shell启动， 找到JDK安装路径，打开bin文件夹， 双击 jconsole。


#### 1.8 hprof
hprof：Heap/CPU Profiling Tool 能够展现CPU使用率，统计堆内存使用情况。  
J2SE 中提供了一个简单的命令行工具来对java程序的cpu和heap进行profiling,叫做HPROF。HPROF实际上是JVM中的一个native库，它会在JVM启动的时候通过命令行参数来动态加载，并成为JVM进程的一部分。若要在java进程启动的时候用使用HPROF，用户可以通过各种命令行参数类型来使用HPROF对java进程的heap或者（和）cpu进行profiling的功能。HPROF产生profiling数据可以是二进制的，也可以是文本格式的。这些日志可以用来跟踪和分析java进程的性能问题和瓶颈，解决内存使用上不优的地方或者程序实现上的不优之处。二进制格式的日志还可以被JVM中的HAT工具来进行浏览和分析，用以观察JAVA进程的heap中各种类型和数据的情况。在J2SE5.0以后的版本中，HPROF已经被并入到一个叫做java Virtual Machine Tool Interface (JVM TI)中。  
**语法格式如下**
```
java -agentlib:hprof[=options] ToBeProfiledClass
java -Xrunprof[:options] ToBeProfiledClass
javac -J-agentlib:hprof[=options] ToBeProfiledClass 
```
**完整的命令选项如下：**
//todo 

### Linux

#### 2.1 Top
Linux 中的top命令显示系统上正在运行的进程。它是系统管理员最重要的工具之一。被广泛用于监视服务器的负载。在本篇中，我们会探索top命令是一个交互命令。在运行top的时候还可以运行很多命令。  
top的使用方法 top [-d number] |top [-bnp]  
**使用方法** 

top -Hp <pid> 可以查看more进程的线程信息
```
-H 显示线程信息
-p 指定pid
```

### MACOS  
#### 3.1 Top  
> 查看cpu占用最高的进程  
> top 
> o  cpu 
> top -Hp <pid>

#### cpu 查看命令
```
查看CPU信息（型号）  
$ cat /proc/cpuinfo | grep name | cut -f2 -d: | uniq -c
查看物理CPU个数  
$ cat /proc/cpuinfo| grep "physical id"| sort| uniq| wc -l
查看每个物理CPU中core的个数(即核数)
$ cat /proc/cpuinfo| grep "cpu cores"| uniq
查看逻辑CPU的个数
$ cat /proc/cpuinfo| grep "processor"| wc -l
```