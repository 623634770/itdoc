## 1.Duplicated Code  
1.Extract Method  
2.Pull Up Method  
3.Form Template Method  
4.Substitute Algorithm  
5.Extract Class  

## 2.Long Method  
1.Extract Method  
2.Replace Temp With Query  
3.Introduce Parameter Object (引入参数对象)  
4.Preserve Whole Object (保持对象完整)  
5.Decompose Conditional(分解条件表达式)  

## 3.Large Class(过大的类)  
1.Extract Class  
2.Extract Subclass  
3.Extract Interface  
4.Duplicate Observed Data  

## 4.Long Parameter List(过长参数列)  
1.Replace Parameter with Method  
2.Preserve Whole Object  
3.Introduce Parameter Object  

## 5.Divergent Change (发散式变化)  
1.Extract Class  变化提炼到另一个类  

## 6.Shotgun Surgery(散弹式修改)  
1.Move Method  
2.Move Field  
3.Inline Class  
 通常使用 1 和 2 所需修改的代码放进同一个类。或可以运用3 吧一系列相关行为放进同一个类。  

## 7.Feature Envy  
1.Move Method  
2.Extract Method  
设计模式：Strategy 和 Visitor  

## 8.Data Clumps  
1.Extract Class  
2.Introduce Parameter Object  
Preserve Whole Object  

## 9.Primitive Obsession  
1.Replace Data Value with Object  
2.Replace Type Code with Class  
3.Replace Type Code with Subclass  
4.Replace Type Code with State/Strategy  
5.Extract Class  
6.Introduce Parameter Object(以类取代类型码)   
7.Replace Array with Object  (以对象取代数组)   

## 10.Switch Statements  
1.Extract Method  
2.Replace Type Code with Subclass  
3.Replace Type Code with State/Strategy  
4.Replace Conditional with Polymorphism  

5.Replace Parameter with Explicit Methods  
6.Introduce Null Object  

## 11.Parallel inheritance Hierarchies(平行继承体系)  
消除策略：让一个继承体系的实例引用另一个继承体系的实例。  
1.Move Method  
2.Move Field  


## 12.Lazy Class(冗赘类)  
1.Collapse Hierarchy(折叠继承体系) [超类和子类之间无太大区别，将它们合为一体]  
2.Inline Class(将类内联化) 将一个内塞入另一个类中  

## 13.Speculative Generality  
1.Collapse Hierarchy  
2.Inline Class  
3.Remove Parameter  
4.Rename Method  

## 14.Temporary Field  
1.Extract Class  
2.Introduce Null Object  
3.Extract Class 提炼相关变量和函数  

## 15.Message Chains  
1.Hide Delegate(隐藏委托关系)  
2.Extract Method  
3.Move Method  

## 16.Middle Man  
1.Remove Middle Man  
2.InlineMethod  
3.Replace Delegation with Inheritance  

## 17.Inappropriate Intimacy  
1.Move Method  
2.Move Field  
3.Change Bidirectional Association to Unidirectional  
4.Extract Class  
5.Hide Delegate  
6.Replace Inheritance with Delegation  


## 18.Alternative Class with Different Interfaces  
1.Rename Method  
2.Move Method  
3.Extract Superclass  

## 19.Incomplete Library Class  
1.Move Method  
2.Introduce Foreign Method(引用外加函数)  
3.Introduce Local Extension  

## 20.Data Class  
1.Encapsulate Field  
2.Encapsulate Collection  
3.Remover Setting Method  
4.Move Method  
5.Extract Method  
6.Hide Method  

## 21.Refused Bequest  
1.Push Down Method  
2.Push Down Field  
3.Replace Inheritance with Delegation  


## 22.Comments  
1.Extract Method  
2.Rename Method  
3.Introduce Assertion  