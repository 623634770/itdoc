# 简化条件表达式
## 1.Decompose Conditional(分解条件表达式)
## 2.Consolidate Conditional Expression(合并条件表达式)
## 3.Consolidate Duplicate Conditional Fragments(合并重复的条件片段)
## 4.Remove Control Flag(移除控制标记)
**以break语句或return语句取代控制标记**
## 5.Replace Nested Conditional with Guard Clauses(以 卫语句 取代嵌套条件表达式)
## 6.Replace Conditional with Polymorphism(以多态取代条件表达式)
## 7.Introduce Null Object(引入Null对象)
## 8.Introduce Assertion(引入断言)