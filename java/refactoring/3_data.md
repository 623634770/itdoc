# 重新组织数据
## 1.Self Encapsulate Field(自封装字段)  
**动机**
直接访问：代码容易阅读。  
间接访问：可以覆盖函数改变获取数据；灵活支持数据管理（延迟初始化） 


## 2.Replace Data Value with Object(以对象取代数据值)  
## 3.Change Value to Reference(将值对象改成引用对象)  

**做法**  
使用Replace Contructor with Factory Method.  


## 4.Change Reference to Value(将引用对象改为值对象)

**做法**  
1.检查重构目标是否为不可变对象，或是否可修改为不可变对象。  
如果不可变，就使用Remove Setting Method,直到它成为不可变的为止。  
如果无法将该对象修改成不可变的，就放弃使用本项重构。  
2.建立equals() 和 hashCode()  


## 5.Replace Array with Object(以对象取代数组)
## 6.Duplicate Observed Data(复制“被监视数据”)
## 7.Change Unidirectional Association to Bidirectional(将单向关联改成双向关联)
## 8.Change Bidirectional Association to Unidirectional(将双向关联改为单向关联)
## 9.Replace Magic Number with Symbolic Constant(以字面常量取代魔法数)
## 10.Encapsulate Field(封装字段)
## 11.Encapsulate Collection(封装集合)
## 12.Replace Record with Data Class(以数据类型取代记录)
## 13.Replace Type Code with Class(以类取代类型码)
## 14.Replace Type Code with Subclasses(以子类取代类型码)
## 15.Replace Type Code with State/Strategy(以State/Strategy取代类型码)
## 16.Replace Subclasses with Field(以字段取代子类)